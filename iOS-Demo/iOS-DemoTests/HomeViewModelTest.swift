//
//  HomeViewModelTest.swift
//  iOS-DemoTests
//
//  Created by Khanh Nguyen on 10/05/2021.
//

import Foundation
import XCTest

class HomeViewModelTest: XCTestCase {

    private var _viewModel: HomeVieWModel!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        _viewModel = HomeVieWModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSum() throws {
        var result = _viewModel.sum(first: 1, second: 2)
        XCTAssertEqual(result, 3, "in correct")
        
        result = _viewModel.sum(first: 0, second: 1)
        XCTAssertEqual(result, 1, "in correct")
        
        result = _viewModel.sum(first: -1, second: -2)
        XCTAssertEqual(result, -3, "in correct")
    }
    
    func testSum2() throws {
        var result = _viewModel.sum(first: 1, second: 1)
        XCTAssertEqual(result, 2, "in correct")
        
        result = _viewModel.sum(first: -1, second: -2)
        XCTAssertEqual(result, -3, "in correct")
    }
    
}
